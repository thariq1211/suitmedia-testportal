'use client'
import Image from 'next/image';
import styles from './ProductCard.module.css';


export default function ProductKnowledge(props) {
    return (
        <div className={styles.card} style={{ ...props.isGift && { height: 'fit-content' } }}>
            <Image src={props.image} alt='image' />
            <div className={styles.badge}>Pet knowledge</div>
            <div className={styles.metadata}>
                <span>What is a Pomeranian? How to Identify Pomeranian Dogs</span>
                <p>The Pomeranian, also known as the Pomeranian (Pom dog), is always in the top of the cutest pets. Not only that, the small, lovely, smart, friendly, and skillful circus dog breed.</p>
            </div>
        </div>
    )
}