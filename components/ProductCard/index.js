'use client'
import Image from 'next/image';
import styles from './ProductCard.module.css';

import { thousandsSeparator } from '@/utils';

export default function ProductCard(props) {
    return (
        <div className={styles.card} style={{ ...props.isGift && { height: 'fit-content' } }}>
            <Image src={props.image} alt={props.title} />
            <div className={styles.metadata}>
                <span>{props.title}</span>
                <p>Gene: <strong>{props.sex}</strong> Age: <strong>{props.age} months</strong></p>
                <span>{thousandsSeparator({ value: props.price })} VND</span>
                {props.isGift && (
                    <div className={styles.gift}>Free Toy & Free Shaker</div>
                )}
            </div>
        </div>
    )
}