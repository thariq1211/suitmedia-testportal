import dog1 from '@/assets/img/dog1.png';
import dog2 from '@/assets/img/dog2.png';
import dog3 from '@/assets/img/dog3.png';
import dog4 from '@/assets/img/dog4.png';
import dog5 from '@/assets/img/dog5.png';
import dog6 from '@/assets/img/dog6.png';
import dog7 from '@/assets/img/dog7.png';
import dog8 from '@/assets/img/dog8.png';

import sellers1 from '@/assets/img/sellers1.png';
import sellers2 from '@/assets/img/sellers2.png';
import sellers3 from '@/assets/img/sellers3.png';
import sellers4 from '@/assets/img/sellers4.png';
import sellers5 from '@/assets/img/sellers5.png';
import sellers6 from '@/assets/img/sellers6.png';
import sellers7 from '@/assets/img/sellers7.png';

export const thousandsSeparator = ({ value = 0, separator = '.' }) => {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator)
}

export const petCatalogues = [
    {
      id: 11,
      image: dog1,
      title: 'MO231 - Pomeranian White',
      sex: 'Male',
      age: '02',
      price: 6900000
    },
    {
      id: 12,
      image: dog2,
      title: 'MO231 - Pomeranian White',
      sex: 'Male',
      age: '02',
      price: 6900000
    },
    {
      id: 13,
      image: dog3,
      title: 'MO231 - Pomeranian White',
      sex: 'Male',
      age: '02',
      price: 6900000
    },
    {
      id: 14,
      image: dog4,
      title: 'MO231 - Pomeranian White',
      sex: 'Male',
      age: '02',
      price: 6900000
    },
    {
      id: 15,
      image: dog5,
      title: 'MO231 - Pomeranian White',
      sex: 'Male',
      age: '02',
      price: 6900000
    },
    {
      id: 16,
      image: dog6,
      title: 'MO231 - Pomeranian White',
      sex: 'Male',
      age: '02',
      price: 6900000
    },
    {
      id: 17,
      image: dog7,
      title: 'MO231 - Pomeranian White',
      sex: 'Male',
      age: '02',
      price: 6900000
    },
    {
      id: 18,
      image: dog8,
      title: 'MO231 - Pomeranian White',
      sex: 'Male',
      age: '02',
      price: 6900000
    },
  ]

export const sellers = [
    sellers1, sellers2, sellers3, sellers4, sellers5, sellers6, sellers7
]