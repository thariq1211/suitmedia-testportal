import Image from "next/image";

import ProductCard from "@/components/ProductCard";
import ProductKnowledge from "@/components/PetKnowledge";

import { petCatalogues, sellers } from "@/utils";

import logo from '@/public/logo.svg';
import vietnamLogo from '@/assets/img/vietnam-flag.svg';
import bannerLogo from '@/assets/img/womangendongdog.png';
import bannerLogo2 from '@/assets/img/handshake.png';
import paw from '@/assets/img/fontisto_paw.png';
import search from '@/assets/img/search.svg';

import fb from '@/assets/img/fb.png'
import twitter from '@/assets/img/twitter.png'
import ig from '@/assets/img/ig.png'
import yt from '@/assets/img/yt.png'

export default function Home() {
  return (
    <main>
      <header>
        <nav className="navbar">
          <div className="container">
            <ul>
              <li>
                <Image src={logo} alt="logo" />
              </li>
              <li><a href="#">Home</a></li>
              <li><a href="#">Category</a></li>
              <li><a href="#">About</a></li>
              <li><a href="#">Contact</a></li>
              <li>
                <form onSubmit={(e) => { e.preventDefault() }}>
                  <div className="form-input">
                    <input style={{ width: 280 }} className="input-search" type="text" placeholder="Search Something here"></input>
                  </div>
                  <button className="btn" type="submit">Join the Community</button>
                </form>
              </li>
              <li className="lang-switch">
                <Image src={vietnamLogo} alt="vietlang" />
                <button className="lang">VND</button>
              </li>
            </ul>
            <div className="mobile-nav">
              <span className="burger"></span>
              <span><Image src={logo} alt="logo" /></span>
              <span><Image src={search} alt="search" /></span>
            </div>
          </div>
        </nav>
        <div className="bigscreen">
          <div className="left-content">
            <div className="innersquare-left" />
            <div className="bottomsquare-left" />
            <div className="content">
              <h2>One More friend</h2>
              <h3>Thousands More Fun!</h3>
              <p style={{ marginBlock: 18 }}>
                Having a pet means you have more joy, a new friend, a happy person who will always be with you to have fun. We have 200+ different pets that can meet your needs!
              </p>
              <div style={{ display: 'flex', gap: 12 }}>
                <button className="btn btn-secondary btn-intro">View Intro</button>
                <button className="btn">Explore Now</button>
              </div>
            </div>
          </div>
          <div className="right-content">
            <div className="square1"></div>
            <div className="square2"></div>
            <div className="mini-square1"></div>
            <div className="mini-square2"></div>
            <div className="mini-square3"></div>
          </div>
        </div>
      </header>
      <section className="pets">
        <div className="heading">
          <div className="left-heading">
            <h4>Whats new?</h4>
            <h3>Take a look at some of our pets</h3>
          </div>
          <div className="right-heading">
            <button className="btn btn-secondary btn-more">View More</button>
          </div>
        </div>
        <div className="products">
          {petCatalogues.map(item => (
            <ProductCard key={item.id} {...item} />
          ))}
        </div>
      </section>
      <section className="banner">
        <div style={{ position: 'relative' }}>
          <Image className="banner-img" src={bannerLogo} alt="women-and-dog" />
        </div>
        <div className="banner-right">
          <h2>One More friend</h2>
          <h4>Thousands More Fun!</h4>
          <p style={{ marginBlock: 18, textAlign: 'end' }}>
            Having a pet means you have more joy, a new friend, a happy person who will always be with you to have fun. We have 200+ different pets that can meet your needs!
          </p>
          <div style={{ display: 'flex', gap: 12, margin: '16px 0' }}>
            <button className="btn btn-secondary btn-intro">View Intro</button>
            <button className="btn">Explore Now</button>
          </div>
        </div>
      </section>
      <section className="pets pet-products">
        <div className="heading">
          <div className="left-heading">
            <h4>Hard to choose right products for your pets?</h4>
            <h3>Our Products</h3>
          </div>
          <div className="right-heading">
            <button className="btn btn-secondary btn-more">View More</button>
          </div>
        </div>
        <div className="products">
          {petCatalogues.map(item => (
            <ProductCard isGift key={item.id} {...item} />
          ))}
        </div>
      </section>
      <section className="pets pet-seller">
        <div className="heading">
          <div className="left-heading-pet-seller">
            <h4>Proud to be part of</h4>
            <h3>Pet Sellers</h3>
          </div>
          <div className="right-heading">
            <button className="btn btn-secondary btn-more">View all our sellers</button>
          </div>
        </div>
        <div className="products">
          {sellers.map((item, idx) => (
            <Image key={idx} src={item} alt="item products" />
          ))}
        </div>
      </section>
      <section className="second-banner">
        <div className="banner-right" style={{ alignItems: 'start' }}>
          <h2 style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', gap: 12 }}>Adoption <span><Image src={paw} alt="paw" /></span></h2>
          <h4>We need help. so do they.</h4>
          <p style={{ marginBlock: 18 }}>
            Adopt a pet and give it a home, <br/>it will be love you back unconditionally.
          </p>
          <div style={{ display: 'flex', gap: 12, margin: '16px 0' }}>
            <button className="btn">Explore Now</button>
            <button className="btn btn-secondary btn-intro">View Intro</button>
          </div>
        </div>
        <div style={{ position: 'relative' }}>
          <Image className="banner-img second" src={bannerLogo2} alt="women-and-dog" />
        </div>
      </section>
      <section className="pets pet-knowledge">
        <div className="heading">
          <div className="left-heading">
            <h4>You already know ?</h4>
            <h3>Useful pet knowledge</h3>
          </div>
          <div className="right-heading">
            <button className="btn btn-secondary btn-more">View More</button>
          </div>
        </div>
        <div className="products">
          {petCatalogues.map(item => (
            <ProductKnowledge key={item.id} {...item} />
          ))}
        </div>
      </section>
      <footer>
        <div className="newsletter">
          <p>Register now so you don&lsquo;t miss our programs</p>
          <form onSubmit={e => e.preventDefault()}>
            <input style={{ width: '75%', color: 'rgba(153, 162, 165, 1)' }} className="input-email" type="email" placeholder="Enter your Email" />
            <button className="subscripe" type="submit">Subscripe Now</button>
          </form>
        </div>
        <nav>
          <ul>
            <li><a>Home</a></li>
            <li><a>Category</a></li>
            <li><a>About</a></li>
            <li><a>Contact</a></li>
          </ul>
          <ul>
            <li><Image src={fb} alt="facebook"/></li>
            <li><Image src={twitter} alt="twitter" /></li>
            <li><Image src={ig} alt="instagram" /></li>
            <li><Image src={yt} alt="youtube" /></li>
          </ul>
        </nav>
        <div className="brand">
          <p>© 2022 Monito. All rights reserved.</p>
          <Image src={logo} alt="logo-petshop" />
          <div>
            <p>Terms of Service</p>
            <p>Privacy Policy</p>
          </div>
        </div>
      </footer>
    </main>
  );
}
